var http = require('http');
var express = require('express');
var jade = require('jade');
var routes = require('./routes');
var app = express();

app.configure(function(){
	app.set('views', __dirname + '/public');
	app.set('view engine', 'jade');
});

app.use(express.static(__dirname + '/public'));
app.use(express.bodyParser());

routes(app);

var port = 9999;

http.createServer(app).listen(port);

console.log("server is listening on ", port);
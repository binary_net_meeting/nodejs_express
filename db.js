module.exports = new DbHandler();

function DbHandler(options){
	var mongo = require("mongodb");
	var server = new mongo.Server("127.0.0.1", 27017,{});
	this.dbName = (typeof options !== "undefined" && options.db) || "test";	
	this.colName = (typeof options !== "undefined" && options.col) || "test";	
	this.db = new mongo.Db(this.dbName, server, {w:1});
}

DbHandler.prototype.insert = function(doc) {
	var self = this;
	this.db.open(function(err, client){
		if (err) throw err;
		self.db.collection(self.colName, function(err, collection){
			collection.insert(doc, {safe: true}, function(err, objs){
				if (err) throw err;
				self.db.close();
				});
		});
	});
};

DbHandler.prototype.findAll = function(callback){
	var self = this;
	var res;
	this.db.open(function(err, client){
		if (err) throw err;
		self.db.collection(self.colName, function(err, collection){
			collection.find(function(error, cursor){
				cursor.toArray(function(errorCur, arr){
					callback(arr);					
					self.db.close();
				});
			});
		});
	});
}
module.exports = function(app){
	var db = require('./db')

	app.get('/', function(req, res){
		res.render('index', {title: '.NET -> Node.js'});
	});


	app.post('/add', function(req, res){
		var val = req.body.val;
		db.insert({a: val});
	});

	app.get('/all', function(req, res){
		var callback = function(info){
			res.render('all', {info:info});
		}
		db.findAll(callback);
	});
};